# How it Works

Stores who you contacted for around 2 weeks. When you or someone you contacted is infected, alerts get sent to you or to people who contacted you. Keeps the UUID of any close-ranged devices acquired through Bluetooth. Only keeps the NDID (Nearlog Device IDentifier) of the device, and the NDID is changed every day.

## Example

![prototype](/en-CA/example.png)
