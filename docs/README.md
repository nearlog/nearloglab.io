---
home: true
heroImage: /icon.svg
features:
- title: Anonymous
  details: Nearlog traces contacts anonymously. Nearlog collects no identifiable user data.
- title: ⚠**Work In Progress**
  details: Nearlog is a *work in progress* details may change and the service/system is *not* complete.
- title: Open source
  details: Nearlog is open source. The source code is available at https://gitlab.com/colourdelete/nearlog_app_f/
//- title: Simple
//  details: Nearlog is simple.
footer: Nearlog by Colourdelete
---

## Prototype

![prototype](/en-CA/prototype.png)

Nearlog Flutter App Pipeline Status: ![Nearlog Flutter App Pipeline Status](https://gitlab.com/colourdelete/nearlog_app_f/badges/master/pipeline.svg?style=flat-square "Nearlog Flutter App Pipeline Status")

Nearlog Site Pipeline Status: ![Nearlog Site Pipeline Status](https://gitlab.com/nearlog/nearlog.gitlab.io/badges/master/pipeline.svg?style=flat-square "Nearlog Site Pipeline Status")
