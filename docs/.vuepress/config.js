module.exports = {
    title: 'Nearlog',
    description: 'Anonymous contact tracing.',
    base: '/',
    dest: 'public',
    locales: {
        '/': {
            lang: 'en-CA',
            title: 'Nearlog',
            description: 'Anonymous contact tracing.'
        },
        '/ja-JP/': {
            lang: 'ja-JP',
            title: 'Nearlog',
            description: '匿名性の高い接触追跡アプリ"ニアログ"'
        }
//   }
    },
    themeConfig: {
        nav: [
            { text: 'Releases', link: '/releases/' },
            { text: 'How it Works', link: '/how-it-works/' },
            { text: 'Q&A', link: '/qaa/' },
            { text: 'Download for Android', link: 'https://play.google.com/apps/testing/com.colourdelete.nearlog_app_f' }
        ],
        locales: {
            '/ja-JP': {
                nav: [
                    { text: 'Releases', link: '/ja-JP/releases/' },
                    { text: 'How it Works', link: '/ja-JP/how-it-works/' },
                    { text: 'Q&A', link: '/ja-JP/qaa/' },
                    { text: 'Download for Android', link: 'https://play.google.com/apps/testing/com.colourdelete.nearlog_app_f' }
                ],
            },
            '/': {
                nav: [
                    { text: 'Releases', link: '/releases/' },
                    { text: 'How it Works', link: '/how-it-works/' },
                    { text: 'Q&A', link: '/qaa/' },
                    { text: 'Download for Android', link: 'https://play.google.com/apps/testing/com.colourdelete.nearlog_app_f' }
                ],
            }
        }
    }
}
