---
title: How it Works
---
# How it Works
Nearlogアプリは、Bluetoothを通して近接した人の記録を2週間だけ保管します。誰かが感染した場合、その感染した人と2週間以内に接触した人に警告が送らます。接触したデバイスの NDID (Nearlog Device IDentifier) を Bluetooth を通じて取得します。記録するのは NDID だけにすることと、NDID を毎時または毎日変更することで匿名性を確保します。
## Example

![prototype](/ja-JP/example.png)
