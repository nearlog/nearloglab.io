---
home: true
heroImage: /icon.svg
features:
- title: 匿名性
  details: Nearlogは匿名性を保ちながら接触を追跡します。Bluetoothを通じて近接したデバイスの一意性のある乱数 (NDID) を交換するため、位置情報などの個人情報を収集しません。
- title: ⚠**開発中**
  details: Nearlogは開発中のテストアプリをGoogle play storeでベータリリースしています。
- title: オープンソース
  details: NearlogのソースコードはGitlabで公開されています。https://gitlab.com/colourdelete/nearlog_app_f/
//- title: シンプル
//  details: Nearlogはシンプルさを追求しています。
footer: Nearlog by Colourdelete
---

## 試作品

![prototype](/ja-JP/prototype.png)

Nearlog Flutter App Pipeline Status: ![Nearlog Flutter App Pipeline Status](https://gitlab.com/colourdelete/nearlog_app_f/badges/master/pipeline.svg?style=flat-square "Nearlog Flutter App Pipeline Status")

Nearlog Site Pipeline Status: ![Nearlog Site Pipeline Status](https://gitlab.com/nearlog/nearlog.gitlab.io/badges/master/pipeline.svg?style=flat-square "Nearlog Site Pipeline Status")
