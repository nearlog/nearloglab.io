---
title: Releases
---

# Releases

## Version 0.2.0 Alpha

![Availability | Closed](https://img.shields.io/badge/Availability-Closed-red?style=flat-square)
![Version | 0.2.0 α](https://img.shields.io/badge/Version-0.2.0%20α-red?style=flat-square)

- [x] Bluetooth Beacon
  - [x] Broadcast
  - [x] Beacon Listen
- [x] Persistent Storage
  - [x] History Persistent Storage
  - [ ] NDID Persistent Storage
- [ ] Server Integration
  - [ ] NDID Search
  - [ ] NDID Control
    - [ ] Submit NDID
    - [ ] Alter NDID Level (e.g. test returned positive)
    - [ ] Delete NDID (e.g. test returned negative)
- [ ] Settings
  - [x] Beacon Settings
  - [ ] Persistent Storage Settings
  - [ ] Server Integration Settings

Versions under `0.2.0` are undocumented because *real* versioning started at `0.2.0`.
